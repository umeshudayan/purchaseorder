var express = require('express')
const app = express();
var route = require('./router')
app.use(express.urlencoded({ extended: false }));
app.use(express.json())
const cors = require('cors');
app.use(cors())

app.use('/api',route)

app.listen(3000,function(){
    console.log("server running....");
})