
const { MongoClient } = require('mongodb');

const url = 'mongodb+srv://umeshudayan15:8078285032@cluster0.sfljuns.mongodb.net/?retryWrites=true&w=majority'; // Replace with your MongoDB connection string
const dbName = 'purchase_order';

async function connectToMongoDB() {
  try {
    const client = await MongoClient.connect(url);
    const db = client.db(dbName);
    console.log('Connected successfully to MongoDB');
    return db;
  } catch (err) {
    console.error('Error connecting to MongoDB:', err);
    throw err;
  }
}

module.exports = connectToMongoDB;

