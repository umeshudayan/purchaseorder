const connectToMongoDB = require('../db');

const collectionName = 'order_details';
const collectionName1 = 'item_details'

module.exports.PurchaseEntryAdded = async (req, res) => {
    try {
        var db = await connectToMongoDB();
        var Ordercollection = db.collection(collectionName);
        var itemcollection = db.collection(collectionName1);


        let { vendor, order_no, order_date, order_location, item_details, order_status } = req.body
        // console.log(vendor, "vendor", item_details);

        if (vendor == '' || vendor == undefined || order_no == '' || order_no == undefined || order_date == undefined || order_date == '' || order_location == '' || order_location == undefined || order_status == '' || order_status == undefined) {
            return res.send({
                result: false,
                message: "insufficient parameters"
            })
        }
        let CheckOrderquery = { order_order_number: order_no };
        let CheckOrder = await Ordercollection.find(CheckOrderquery).toArray();
        // console.log(CheckOrder, "CheckOrder");
        // let CheckOrder = await PurchaseEntryModel.CheckinGOrder(order_no)


        if (CheckOrder.length > 0) {

            var UpdateOrderQuery = {
                $set: {
                    order_vendor: vendor,
                    order_order_date: order_date,
                    order_location: order_location,
                    order_status: order_status,
                },
            };
            let CheckOrderquery = { order_order_number: order_no };
            var UpdateOrder = await Ordercollection.updateOne(CheckOrderquery, UpdateOrderQuery);
            
            var updatedOrderDocument = await Ordercollection.find(CheckOrderquery).toArray();
            // console.log(updatedOrderDocument,"updated");
            // let UpdateOrder = await PurchaseEntryModel.UpdatingOrder(vendor, order_no, order_date, order_location, order_status)
            if (item_details.length !== 0 && item_details !== undefined) {
                console.log("chekc");
                let CheckItemquery = {item_order_no: order_no };
                var result = await itemcollection.deleteMany(CheckItemquery);
                console.log('Deleted documents count:', result.deletedCount)
                // let DeleteOldPurchaseItem = await PurchaseEntryModel.DeletingItem(UpdateOrder.rows[0].order_id)
                item_details.forEach(async (element) => {
                    if (element.item_product == '' || element.item_product == undefined || element.item_quantity == '' || element.item_quantity == undefined || element.item_amount == undefined || element.item_amount == '' || element.item_discount == '' || element.item_discount == undefined || element.item_tax == '' || element.item_tax == undefined || element.item_total == '' || element.item_total == undefined) {
                        return res.send({
                            result: false,
                            message: "insufficient parameters"
                        })
                    }

                    var newItemQuery = [{
                        item_order_no: order_no,
                        item_product: element.item_product,
                        item_quantity: element.item_quantity,
                        item_amout: element.item_amount,
                        item_discount: element.item_discount,
                        item_tax: element.item_tax,
                        item_total: element.item_total
                      }];

                      var newItem = await itemcollection.insertMany(newItemQuery);
                    // let itemInsert = await PurchaseEntryModel.InsertIntoItem(UpdateOrder.rows[0].order_id, element)
                });
            }
        } else {
            var OrderInsertQuery = {
                order_vendor: vendor,
                order_order_number: order_no,
                order_order_date: order_date,
                order_location: order_location,
                order_status: order_status,
            };
            var OrderInsert = await Ordercollection.insertOne(OrderInsertQuery);
            var InsertedOrderDocument = await Ordercollection.find({ order_order_number: order_no }).toArray();
            // console.log(InsertedOrderDocument,"asss");
            // let OrderInsert = await PurchaseEntryModel.InsertIntoOrder(vendor, order_no, order_date, order_location, order_status)
            if (item_details.length !== 0 && item_details !== undefined) {
                item_details.forEach(async (element) => {
                    if (element.item_product == '' || element.item_product == undefined || element.item_quantity == '' || element.item_quantity == undefined || element.item_amount == undefined || element.item_amount == '' || element.item_discount == '' || element.item_discount == undefined || element.item_tax == '' || element.item_tax == undefined || element.item_total == '' || element.item_total == undefined) {
                        return res.send({
                            result: false,
                            message: "insufficient parameters"
                        })
                    }
                    var newItemQuery = [{
                        item_order_no: order_no,
                        item_product: element.item_product,
                        item_quantity: element.item_quantity,
                        item_amout: element.item_amount,
                        item_discount: element.item_discount,
                        item_tax: element.item_tax,
                        item_total: element.item_total
                      }];

                      var newItem = await itemcollection.insertMany(newItemQuery);
                    // let itemInsert = await PurchaseEntryModel.InsertIntoItem(OrderInsert.rows[0].order_id, element)
                });
            }
        }


        return res.send({
            result: true,
            message: `purchase order ${order_status} successfully`
        })


    } catch (error) {
        return res.send({
            error: error.message,
            errorStack: error.stack
        });
    }


}
